const config = require("./config/db_config.json");
const express = require("express");
const bodyParser = require("body-parser");
const api = require("./routes/api");
const auth = require("./routes/auth");

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//routing
app.use("/api", api);
app.use("/auth", auth);

//run server
const port = config.local.port;
const server = app.listen(process.env.PORT || port, () => {
  console.log("this application uses port: " + server.address().port);
});

//home route
app.all("*", function(req, res) {
  res.status(404);
  res.json({
    description: "appartments api"
  });
});

module.exports = app;
