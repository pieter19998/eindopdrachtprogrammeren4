const config = require("../config/db_config.json");
const moment = require("moment");
const jwt = require("jwt-simple");

//
// Encode (van email naar token)
//

//get userId en email to insert into token and set experation date
function encodeToken(email , userId) {
  const playload = {
    exp: moment()
      .add(10, "days")
      .unix(),
    iat: moment().unix(),
    email: email,
    userId: userId
  };
  return jwt.encode(playload, config.local.key);
}

//
// Decode (van token naar email)
//
function decodeToken(token, cb) {
  try {
    const payload = jwt.decode(token, config.local.key);

    // Check if the token has expired
    if (moment().unix() > payload.exp) {
      cb(new Error("token_has_expired"));
    } else {
      cb(null, payload);
    }
  } catch (err) {
    cb(err, null);
  }
}

module.exports = {
  encodeToken,
  decodeToken
};
