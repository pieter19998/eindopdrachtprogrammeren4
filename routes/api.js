const express = require("express");
const assert = require("assert");
const db = require("../db/mysql-connector");
const Appartment = require("../models/apartment");
const Reservation = require("../models/reservation");
const jwt = require("../helpers/jwt");
const router = express.Router();

var _userId = null;

//get the login token from the request heaader
router.all("*", function(req, res, next) {
  //get token
  const token = req.header("token");

  jwt.decodeToken(token, (err, payload) => {
    if (err) {
      console.log("Error handler: " + err.message);
      res.status(401);
      res.json({
        error: err.message
      });
    } else {
      //save user id for authorisation
      _userId = payload["userId"];
      next();
    }
  });
});

//get all data from apartment including reservationand user data
router.get("/appartments", function(req, res, next) {
  try {
    // create new query
    const query = {
      sql:
        "select a.*, u.firstname, u.lastname, r.startdate, r.enddate, r.status from apartment as a join user as u on u.userid = a.userid join reservation as r on r.apartmentid = a.apartmentid",
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(res.status(404), res.json({ error: err.message }));
      } else {
        if (rows === undefined || rows.length == 0) {
          res.status(404),
            res.json({
              error: "no appartments found in database"
            });
        } else {
          res.json(rows);
        }
      }
    });
  } catch (ex) {
    next(ex);
  }
});

//get single data from apartment including reservationand user data from given ID
router.get("/appartments/:id", function(req, res, next) {
  try {
    const id = req.params.id;
    // create new query
    const query = {
      sql:
        "select a.*, u.firstname, u.lastname, r.startdate, r.enddate, r.status from apartment as a join user as u on u.userid = a.userid join reservation as r on r.apartmentid = a.apartmentid WHERE a.apartmentid = ?;",
      timeout: 2000
    };

    // Perform query
    db.query(query, id, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(res.status(404), res.json({ error: err.message }));
      } else {
        if (rows === undefined || rows.length == 0) {
          res.status(404),
            res.json({
              error: "no appartments found with id: " + req.params.id
            });
        } else {
          res.status(200).json(rows);
        }
      }
    });
  } catch (ex) {
    next(ex);
  }
});

//get all reservations with given appartment id
router.get("/appartments/:id/reservations", function(req, res, next) {
  try {
    const id = req.params.id;
    // create new query
    const query = {
      sql: "SELECT * FROM `reservation` WHERE apartmentId = ?",
      timeout: 2000
    };

    // Perform query
    db.query(query, id, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(res.status(404), res.json({ error: err.message }));
      } else {
        if (rows === undefined || rows.length == 0) {
          res.status(404),
            res.json({
              error:
                "no reservations found with appartment id: " + req.params.id
            });
        } else {
          res.status(200).json(rows);
        }
      }
    });
  } catch (ex) {
    next(ex);
  }
});

//get all reservations with given appartment id and reservation id
router.get("/appartments/:appartmentId/reservations/:reservationId", function(
  req,
  res,
  next
) {
  try {
    const appartmentId = req.params.appartmentId;
    const reservationId = req.params.reservationId;
    // create new query
    const query = {
      sql:
        "SELECT * FROM `reservation` WHERE apartmentId = ? AND reservationId = ?",
      timeout: 2000
    };
    // Perform query
    db.query(query, [appartmentId, reservationId], (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(res.status(404), res.json({ error: err.message }));
      } else {
        if (rows === undefined || rows.length == 0) {
          res.status(404),
            res.json({
              error:
                "no reservations found with appartment id: " +
                req.params.id +
                "and reservation id: " +
                req.params.reservationId
            });
        } else {
          res.status(200).json(rows);
        }
      }
    });
  } catch (ex) {
    next(ex);
  }
});

//
//POST CALLS
//

// create new apartment
router.post("/appartments", function(req, res, next) {
  try {
    //check type
    // assert(typeof req.body.apartmentId === 'number',"apartmentId is not a integer!");
    assert(
      typeof req.body.description === "string",
      "apartmentId is not a string!"
    );
    assert(
      typeof req.body.streetAddress === "string",
      "apartmentId is not a string!"
    );
    assert(
      typeof req.body.postalCode === "string",
      "apartmentId is not a string!"
    );
    assert(typeof req.body.city === "string", "apartmentId is not a string!");
    // assert(typeof req.body.userId === "integer","apartmentId is not a integer!");
    const _appartment = new Appartment(
      req.body.apartmentId,
      req.body.description,
      req.body.streetAddress,
      req.body.postalCode,
      req.body.city,
      _userId
    );
    // create new query
    const query = {
      sql:
        "INSERT INTO `apartment`(apartmentId, description, streetAddress, postalCode, city, UserId) VALUES (?,?,?,?,?,?)",
      values: [
        _appartment.apartmentId,
        _appartment.description,
        _appartment.streetAddress,
        _appartment.postalCode,
        _appartment.city,
        _appartment.userId
      ],
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(res.status(400), res.json({ error: err.message }));
      } else {
        res.status(200).json({ description: "succesfully created apartment" });
      }
    });
  } catch (ex) {
    next(ex);
  }
});

//create new reservation
router.post("/appartments/:id/reservations", function(req, res, next) {
  try {
    // assert(typeof req.body.userId === "integer","apartmentId is not a integer!");
    const _reservation = new Reservation(
      req.body.reservationId,
      req.body.startDate,
      req.body.endDate,
      req.body.status,
      _userId,
      req.params.id
    );

    // create new query
    const query = {
      sql:
        "INSERT INTO `reservation`(reservationId, startDate, endDate, status, userId, apartmentId) VALUES (?,?,?,?,?,?)",
      values: [
        _reservation.reservationId,
        _reservation.startDate,
        _reservation.endDate,
        _reservation.status,
        _reservation.userId,
        _reservation.apartmentId
      ],
      apartmantId: [_reservation.apartmentId],
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(res.status(400), res.json({ error: err.message }));
      } else {
        res
          .status(200)
          .json({ description: "succesfully created reseravtion" });
      }
    });
  } catch (ex) {
    next(ex);
  }
});

//
//UPDATE//
//

//update apartment with given ID
router.put("/appartments/:id/", function(req, res, next) {
  try {
    assert(
      typeof req.body.description === "string",
      "description is not a string!"
    );
    assert(
      typeof req.body.streetAddress === "string",
      "streetAddress is not a string!"
    );
    assert(
      typeof req.body.postalCode === "string",
      "postalCode is not a string!"
    );
    assert(typeof req.body.city === "string", "city is not a string!");

    const id = req.params.id;
    const _appartment = new Appartment(
      id,
      req.body.description,
      req.body.streetAddress,
      req.body.postalCode,
      req.body.city,
      _userId
    );
    // create new query
    const query = {
      sql:
        "UPDATE`apartment`SET description = ?, streetAddress = ?, postalCode = ?, city = ? WHERE apartmentId = ?;",
      values: [
        _appartment.description,
        _appartment.streetAddress,
        _appartment.postalCode,
        _appartment.city,
        _appartment.apartmentId
      ],
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(res.status(400), res.json({ error: err.message }));
      } else {
        res.status(200).json({
          description: "succesfully updated apartmen with id: " + req.params.id
        });
      }
    });
  } catch (ex) {
    next(ex);
  }
});

//update reservation with given apartment and reservation id
router.put("/appartments/:apartmantId/reservations/:reservationId", function(
  req,
  res,
  next
) {
  try {
    //check type
    assert(typeof req.body.status === "string", "apartmentId is not a string!");
    //check type
    const _reservation = new Reservation(
      req.params.reservationId,
      req.body.startDate,
      req.body.endDate,
      req.body.status,
      _userId,
      req.params.apartmantId,
    );
    // create new query
    const query = {
      sql:
        "UPDATE reservation SET startDate = ?, endDate = ?, status = ? WHERE apartmentId = ? AND reservationId = ? AND userId = ?",
      values: [
        _reservation.startDate,
        _reservation.endDate,
        _reservation.status,
        _reservation.apartmentId,
        _reservation.reservationId,
        _userId
      ],
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(res.status(400), res.json({ error: err.message }));
      } else {
        if (rows === undefined || rows.affectedRows == 0) {
          res.status(404),
            res.json({
              error:
                "check if reservation and apartmen exits exist and your the owner of the reservation: " +
                req.params.reservationId
            });
        } else {
          res.status(200).json({
            description:
              "succesfully updated apartmen with apartmantId: " +
              req.params.apartmantId +
              " and reservationId: " +
              req.params.reservationId
          });
        }
      }
    });
  } catch (ex) {
    next(ex);
  }
});

//delete reservation
router.delete("/appartments/:id", function(req, res, next) {
  try {
    // create new query
    const query = {
      sql: "DELETE FROM apartment WHERE apartmentId = ? and userId = ?",
      values: [req.params.id, _userId],
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(err.message);
      } else {
        console.log(fields);

        if (rows === undefined || rows.affectedRows == 0) {
          res.status(404),
            res.json({
              error:
                "check if apartment exist and your the owner of the apartment: " +
                req.params.id
            });
        } else {
          res.status(200).json({
            description: "Succesfully deleted apartment and reservation"
          });
        }
      }
    });
  } catch (ex) {
    next(ex);
  }
});

//delete reservation and appartment
router.delete(
  "/appartments/:appartmentId/reservations/:reservationId",
  function(req, res, next) {
    try {
      // create new query
      const queryReservation = {
        sql: "DELETE FROM reservation WHERE reservationId = ? AND userId = ?",
        values: [req.params.reservationId, _userId],
        timeout: 2000
      };

      // Perform query
      db.query(queryReservation, (err, rows, fields) => {
        if (err) {
          console.log(err);
          next(res.status(404), res.json({ error: err.message }));
        }
      });

      // create new query
      const query = {
        sql: "DELETE FROM apartment WHERE apartmentId = ? AND userId = ?",
        values: [req.params.appartmentId, _userId],
        timeout: 2000
      };

      // Perform query
      db.query(query, (err, rows, fields) => {
        if (err) {
          console.log(err);
          next(err.message);
        } else {
          console.log(query);
          if (rows === undefined || rows.affectedRows == 0) {
            res.status(404), console.log(_userId);
            res.json({
              error:
                "check if apartment exist and your the owner of the apartment"
            });
          } else {
            res.status(200).json({
              description: "Succesfully deleted apartment and reservation"
            });
          }
        }
      });
    } catch (ex) {
      next(ex);
    }
  }
);
// fallback
router.all("*", function(req, res) {
  res.status(404);
  res.json({
    description: "Unknown endpoint"
  });
});

module.exports = router;
