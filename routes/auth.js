const express = require("express");
const assert = require("assert");
const router = express.Router();
const User = require("../models/user");
const db = require("../db/mysql-connector");
const bcrypt = require("bcryptjs");
const jwt = require("../helpers/jwt");

//create new user
router.post("/register", function(req, res, next) {
  try {
    // Check type
    assert(typeof req.body.firstname === "string", "firstname is not a string!");
    assert(typeof req.body.lastname === "string", "lastname is not a string!");
    assert(typeof req.body.streetAddress === "string", "streetAddress is not a string!");
    assert(typeof req.body.postalCode === "string", "postalCode is not a string!");
    assert(typeof req.body.city === "string", "city is not a string!");
    assert(typeof req.body.phoneNumber === "string", "phoneNumber is not a integer!");
    assert(typeof req.body.email === "string", "email is not a string!");
    assert(typeof req.body.password === "string", "password is not a string!");

    //hash password
    const salt = 10;
    const passwordhash = bcrypt.hashSync(req.body.password, salt);
    //new user object
    const user = new User(
      req.body.userId,
      req.body.firstname,
      req.body.lastname,
      req.body.streetAddress,
      req.body.postalCode,
      req.body.city,
      req.body.dateOfBirth,
      req.body.phoneNumber,
      req.body.email,
      passwordhash
    );

    // create new query
    const query = {
      sql:
        "INSERT INTO `user`(userId, firstname, lastname, streetAddress, postalCode, city, dataOfBirth, phoneNumber, emailAddress, password) VALUES (?,?,?,?,?,?,?,?,?,?)",
      values: [user.userId, user.firstname, user.lastname, user.streetAddress, user.postalCode, user.city, user.dateOfBirth, user.phoneNumber, user.email, user.password],
      timeout: 2000
    };

    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        console.log(err);
        next(err);
      } else {
        res.status(200).json({ description : "Succesfully created new user: " + user.firstname +" "+ user.lastname});
      }
    });
  } catch (ex) {
    next(ex);
  }
});

//login using username and password
router.post("/login", function(req, res, next) {
  try {
    // Validate with assert if string
    assert(typeof req.body.password === "string", "Password is not a string!");
    assert(typeof req.body.email === "string", "email is not a string!");

    // get the password that is saved in the database
    const query = {
      sql: "SELECT `password` , userId  FROM `user` WHERE `EmailAddress`=?",
      values: [req.body.email],
      timeout: 2000
    };
    // Perform query
    db.query(query, (err, rows, fields) => {
      if (err) {
        next(err);
      } else {
        if (
          rows.length === 1 &&
          bcrypt.compareSync(req.body.password, rows[0].password)
        ) {
          token = jwt.encodeToken(req.body.email, rows[0].userId);
          res.status(200).json({ token: token });
        } else {
          // next(new Error("Invalid login"));
          next(res.status(401), res.json({ error: "Invalid login" }));
        }
      }
    });
  } catch (ex) {
    next(ex);
  }
});

// Fall back
router.all("*", function(req, res) {
  res.status(404);
  res.json({
    description: "Unknown endpoint"
  });
});

module.exports = router;