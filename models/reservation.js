class Reservation
{
    constructor(reservationId,startDate,endDate,status,userId,apartmentId)
    {
        this.reservationId = reservationId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.userId = userId;
        this.apartmentId = apartmentId;
    }
}
module.exports = Reservation;