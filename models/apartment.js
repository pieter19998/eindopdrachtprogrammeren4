class Appartment {
  constructor(
    apartmentId,
    description,
    streetAddress,
    postalCode,
    city,
    userId
  ) {
    this.apartmentId = apartmentId;
    this.description = description;
    this.streetAddress = streetAddress;
    this.postalCode = this.validatePostalCode(postalCode);
    this.city = city;
    this.userId = userId;
  }

  validatePostalCode(postalcode) {
    //check if postal code is valid dutch postal code
    var regex = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;

    if (regex.test(postalcode)) {
      return postalcode;
    } else {
      throw new Error("Invalid postalcode: " + postalcode);
    }
  }
}
module.exports = Appartment;
