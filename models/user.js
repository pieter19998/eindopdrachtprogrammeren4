class User {
  constructor(
    userId,
    firstname,
    lastname,
    streetAddress,
    postalCode,
    city,
    dateOfBirth,
    phoneNumber,
    email,
    password
  ) {
    this.userId = userId;
    this.firstname = firstname;
    this.lastname = lastname;
    this.streetAddress = streetAddress;
    this.city = city;
    this.dateOfBirth = dateOfBirth;
    this.password = password;
    (this.email = this.validateEmail(email)),
      (this.phoneNumber = this.validatePhoneNumber(phoneNumber)),
      (this.postalCode = this.validatePostalCode(postalCode));
  }

  //check if email is valid
  validateEmail(email) {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (regex.test(email)) {
      return email;
    } else {
      throw new Error("Invalid email: " + email);
    }
  }

  //check if email is valid dutch phonenumber
  validatePhoneNumber(phoneNumber) {
    var regex = /^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[1-9]((\s|\s?\-\s?)?[0-9])((\s|\s?-\s?)?[0-9])((\s|\s?-\s?)?[0-9])\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]$/;

    if (regex.test(phoneNumber)) {
      return phoneNumber;
    } else {
      throw new Error("Invalid phoneNumber: " + phoneNumber);
    }
  }

  //check if postal code is valid dutch postal code
  validatePostalCode(postalcode) {
    var regex = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;

    if (regex.test(postalcode)) {
      return postalcode;
    } else {
      throw new Error("Invalid postalcode: " + postalcode);
    }
  }
}
module.exports = User;