var chai = require("chai");
var chaiHttp = require("chai-http");
var server = require("../index.js");
const token =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTgyOTQ5MjUsImlhdCI6MTU1Nzg2MjkyNSwiZW1haWwiOiJ0ZXN0QHRlc3RlbWFpbC5jb20iLCJ1c2VySWQiOjk5OX0.luHpGCY4pOanNaKNqKyOBKAX8R-JuuG-6ZRVttUdS9w";
chai.should();

chai.use(chaiHttp);
var expect = chai.expect;

describe("Unknown endpoint", () => {
  it("Endpoint not found", function(done) {
    chai
      .request(server)
      .get("/oof")
      .end(function(err, res) {
        res.should.have.status(404);
        done();
      });
  });
});

describe("User", () => {
  it("Create new user invalid email", function(done) {
    chai
      .request(server)
      .post("/auth/register")
      .set("Content-Type", "application/json")
      .send({
        userId: 999,
        firstname: "John",
        lastname: "Smith",
        streetAddress: "stationstraat12",
        postalCode: "4374PF",
        city: "Dorderecht",
        dateOfBirth: "14-02-1990",
        phoneNumber: "0181-925624",
        email: "test11testemailcom",
        password: "123"
      })
      .end(function(err, res, body) {
        res.should.have.status(500);
        done();
      });
  });

  it("Create new user invalid dutch phonenumber", function(done) {
    chai
      .request(server)
      .post("/auth/register")
      .set("Content-Type", "application/json")
      .send({
        userId: 999,
        firstname: "John",
        lastname: "Smith",
        streetAddress: "stationstraat12",
        postalCode: "4374PF",
        city: "Dorderecht",
        dateOfBirth: "14-02-1990",
        phoneNumber: "018132925624",
        email: "test@testemail.com",
        password: "123"
      })
      .end(function(err, res, body) {
        res.should.have.status(500);
        done();
      });
  });
  it("Create new user and post", function(done) {
    chai
      .request(server)
      .post("/auth/register")
      .set("Content-Type", "application/json")
      .send({
        userId: 999,
        firstname: "John",
        lastname: "Smith",
        streetAddress: "stationstraat12",
        postalCode: "4374PF",
        city: "Dorderecht",
        dateOfBirth: "14-02-1990",
        phoneNumber: "0181-925624",
        email: "test@testemail.com",
        password: "123"
      })
      .end(function(err, res, body) {
        res.should.have.status(200);
        done();
      });
  });

  describe("User", () => {
    it("Create new user and post duplicate", function(done) {
      chai
        .request(server)
        .post("/auth/register")
        .set("Content-Type", "application/json")
        .send({
          userId: 999,
          firstname: "John",
          lastname: "Smith",
          streetAddress: "stationstraat12",
          postalCode: "4374PF",
          city: "Dorderecht",
          dateOfBirth: "14-02-1990",
          phoneNumber: "0181-925624",
          email: "test@testemail.com",
          password: "123"
        })
        .end(function(err, res, body) {
          res.should.have.status(500);
          done();
        });
    });

    it("User login wrong credentials", function(done) {
      chai
        .request(server)
        .post("/auth/login")
        .set("content-type", "application/json")
        .send({
          email: "test123@testemail.com",
          password: "123444"
        })
        .end(function(err, res, body) {
          res.should.have.status(401);
          done();
        });
    });

    it("User login", function(done) {
      chai
        .request(server)
        .post("/auth/login")
        .set("content-type", "application/json")
        .send({
          email: "test@testemail.com",
          password: "123"
        })
        .end(function(err, res, body) {
          res.should.have.status(200);
          done();
        });
    });
  });
});

describe("Appartment", () => {
  it("no token supplied", function(done) {
    chai
      .request(server)
      .get("/api/appartments")
      .end(function(err, res) {
        res.should.have.status(401);
        done();
      });
  });

  //
  //POST TEST
  //
  it("Create new Apartment", function(done) {
    chai
      .request(server)
      .post("/api/appartments")
      .set("token", token)
      .send({
        apartmentId: 1001,
        description: "mooi nieuw apartement in Dorderecht",
        streetAddress: "macdonaldsstraat123",
        postalCode: "4235FV",
        city: "Dorderecht",
        userId: 999
      })
      .end(function(err, res, body) {
        res.should.have.status(200);
        done();
      });
  });

    it("Create new Apartment for delete apartment test", function(done) {
      chai
        .request(server)
        .post("/api/appartments")
        .set("token", token)
        .send({
          apartmentId: 10009,
          description: "mooi nieuw apartement in Dorderecht",
          streetAddress: "macdonaldsstraat123",
          postalCode: "4235FV",
          city: "Dorderecht",
          userId: 999
        })
        .end(function(err, res, body) {
          res.should.have.status(200);
          done();
        });
    });


  it("Create Failed non valid Postalcode", function(done) {
    chai
      .request(server)
      .post("/api/appartments")
      .set("token", token)
      .send({
        apartmentId: 1001,
        description: "mooi nieuw apartement in Dorderecht",
        streetAddress: "macdonaldsstraat123",
        postalCode: "4235FVFFFFFFF",
        city: "Dorderecht",
        userId: 999
      })
      .end(function(err, res, body) {
        res.should.have.status(500);
        done();
      });
  });

  it("Create new Reservation for Apartment", function(done) {
    chai
      .request(server)
      .post("/api/appartments/1001/reservations")
      .set("token", token)
      .send({
        reservationId: 2002,
        startDate: "2019-01-20",
        endDate: "2020-01-30",
        status: "reserved",
        userId: 999,
        apartmentId: 1001
      })
      .end(function(err, res, body) {
        res.should.have.status(200);
        done();
      });
  });

  it("Create Failed Reservation for Apartment", function(done) {
    chai
      .request(server)
      .post("/api/appartments/1001/reservations")
      .set("token", token)
      .send({
        reservationId: 2002,
        userId: 999,
        apartmentId: 1001
      })
      .end(function(err, res, body) {
        res.should.have.status(400);
        done();
      });
  });

  //
  //Update TESTS
  //
  it("get appartment with id=1001 and update", function(done) {
    chai
      .request(server)
      .put("/api/appartments/1001")
      .set("token", token)
      .send({
        apartmentId: 1001,
        description: "lelijk nieuw apartement in Dorderecht",
        streetAddress: "macdonaldsstraat123",
        postalCode: "4235FV",
        city: "Dorderecht"
      })
      .end(function(err, res, body) {
        res.should.have.status(200);
        done();
      });
  });

  it("get appartment with id=1001 and reservation id=2002 update", function(done) {
    chai
      .request(server)
      .put("/api/appartments/1001/reservations/2002")
      .set("token", token)
      .send({
        // reservationId: 2002,
        startDate: "2019-01-20",
        endDate: "2020-01-30",
        status: "open",
        // apartmentId: 1001
      })
      .end(function(err, res, body) {
        res.should.have.status(200);
        done();
      });
  });

  //
  //GET TESTS
  //
  it("get all reservation aparartment en user data", function(done) {
    chai
      .request(server)
      .get("/api/appartments")
      .set("token", token)
      .end(function(err, res) {
        res.should.have.status(200);
        done();
      });
  });

  it("get apartment with id=1001", function(done) {
    chai
      .request(server)
      .get("/api/appartments/1001")
      .set("token", token)
      .end(function(err, res) {
        res.should.have.status(200);
        res.body.should.be.a("array");
        expect({
          ApartmentId: 1001
        }).to.deep.equal({
          ApartmentId: 1001
        });
        expect({
          Description: "lelijk nieuw apartement in Dorderecht"
        }).to.deep.equal({
          Description: "lelijk nieuw apartement in Dorderecht"
        });
        expect({
          StreetAddress: "macdonaldsstraat123"
        }).to.deep.equal({
          StreetAddress: "macdonaldsstraat123"
        });
        expect({
          PostalCode: "4235FV"
        }).to.deep.equal({
          PostalCode: "4235FV"
        });
        expect({
          City: "Dorderecht"
        }).to.deep.equal({
          City: "Dorderecht"
        });
        expect({
          UserId: 999
        }).to.deep.equal({
          UserId: 999
        });
        expect({
          firstname: "John"
        }).to.deep.equal({
          firstname: "John"
        });
        expect({
          lastname: "Smith"
        }).to.deep.equal({
          lastname: "Smith"
        });
        expect({
          startdate: "2019-01-19T23:00:00.000Z"
        }).to.deep.equal({
          startdate: "2019-01-19T23:00:00.000Z"
        });
        expect({
          enddate: "2020-01-29T23:00:00.000Z"
        }).to.deep.equal({
          enddate: "2020-01-29T23:00:00.000Z"
        });
        expect({
          status: "reserved"
        }).to.deep.equal({
          status: "reserved"
        });
        done();
      });
  });

  it("get non existing apartment id=999999999", function(done) {
    chai
      .request(server)
      .get("/api/appartments/999999999")
      .set("token", token)
      .end(function(err, res) {
        res.should.have.status(404);
        res.body.should.be.a("object");
        expect({
          error: "no appartments found with id: " + "999999999"
        }).to.deep.equal({
          error: "no appartments found with id: " + "999999999"
        });
        done();
      });
  });

  it("get apartment id=1001 with all reservation", function(done) {
    chai
      .request(server)
      .get("/api/appartments/1001/reservations")
      .set("token", token)
      .end(function(err, res) {
        res.should.have.status(200);
        res.body.should.be.a("array");
        done();
      });
  });

  it("get apartment id=1001 with reservation id=2002 ", function(done) {
    chai
      .request(server)
      .get("/api/appartments/1001/reservations/2002")
      .set("token", token)
      .end(function(err, res) {
        res.should.have.status(200);
        res.body.should.be.a("array");
        done();
      });
  });

  //
  //DELETE TESTS
  //
  it("delete apartment id=1001 with reservation id=2002 ", function(done) {
    chai
      .request(server)
      .delete("/api/appartments/1001/reservations/2002")
      .set("token", token)
      .end(function(err, res) {
        res.should.have.status(200);
        done();
      });
  });

  it("Delete appartment with given id: 10009", function(done) {
    chai
      .request(server)
      .delete("/api/appartments/10009")
      .set("token", token)
      .end(function(err, res) {
        res.should.have.status(200);
        done();
      });
  });
});
